package com.liang.xiao.hbase;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HBaseAdmin;

import java.io.IOException;

/**
 * @describe: Hbase操作
 * @author: xiaoliang.liu
 * @date: 2018/5/14 22:57  v1.0
 */
public class HbaseOperate {


    public static void main(String[] args) throws IOException {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "itcast04:2181,itcast05:2181,itcast06:2181");


        HBaseAdmin admin = new HBaseAdmin(conf);

        HTableDescriptor peoples = new HTableDescriptor(TableName.valueOf("peoples"));

        HColumnDescriptor hcd_info = new HColumnDescriptor("info");
        hcd_info.setMaxVersions(3);

        HColumnDescriptor hcd_data = new HColumnDescriptor("data");


        peoples.addFamily(hcd_info);
        peoples.addFamily(hcd_data);

        admin.createTable(peoples);
        admin.close();

    }


}

package com.liang.xiao.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @describe: 统计文本中的单词出现的次数Reduce
 * @author: xiaoliang.liu
 * @date: 2018/4/21 14:35  v1.0
 */
public class WCReducer extends Reducer<Text, LongWritable, Text, LongWritable> {


    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        // 定义一个变量存储单词出现的次数
        Long count = 0L;

        // 循环相加单词的次数
        for (LongWritable value : values) {
            count += value.get();
        }

        // 输出结果
        context.write(key, new LongWritable(count));
    }
}

package com.liang.xiao.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @describe: 统计文本中的单词出现的次数的Map
 * @author: xiaoliang.liu
 * @date: 2018/4/21 14:26  v1.0
 */
public class WCMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 接收数据
        String line = value.toString();
        // 分割数据
        String[] result = line.split(" ");
        // 循环输出数据
        for (String word : result) {
            context.write(new Text(word), new LongWritable(1));
        }
    }
}

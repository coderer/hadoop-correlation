package com.liang.xiao.sumstep;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @describe: 统计和排序好后的Bean
 * @author: xiaoliang.liu
 * @date: 2018/4/29 13:22  v1.0
 */
public class InfoBean implements WritableComparable<InfoBean> {

    /**
     * 账号
     */
    private String account;

    /**
     * 收入
     */
    private double income;

    /**
     * 支出
     */
    private double expense;

    /**
     * 结余
     */
    private double surplus;


    public void set(String account, double income, double expense) {
        this.account = account;
        this.income = income;
        this.expense = expense;
        this.surplus = income - expense;
    }


    /**
     * 比较规则
     */
    public int compareTo(InfoBean info) {

        if (this.income == info.income) {
            return this.expense > info.expense ? 1 : -1;
        } else {
            return this.income > info.income ? -1 : 1;
        }
    }

    /**
     * 序列化
     */
    public void write(DataOutput out) throws IOException {
        out.writeUTF(account);
        out.writeDouble(income);
        out.writeDouble(expense);
        out.writeDouble(surplus);
    }

    /**
     * 反序列化
     */
    public void readFields(DataInput in) throws IOException {
        this.account = in.readUTF();
        this.income = in.readDouble();
        this.expense = in.readDouble();
        this.surplus = in.readDouble();
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getExpense() {
        return expense;
    }

    public void setExpense(double expense) {
        this.expense = expense;
    }

    public double getSurplus() {
        return surplus;
    }

    public void setSurplus(double surplus) {
        this.surplus = surplus;
    }

    @Override
    public String toString() {
        return this.income + "\t" + this.expense + "\t" + this.surplus;
    }
}

package com.liang.xiao.inversesort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @describe: 倒排列表练习
 * @author: xiaoliang.liu
 * @date: 2018/4/29 15:59  v1.0
 */
public class InverseIndex {

    public static void main(String[] args) throws Exception {
        Job job = Job.getInstance(new Configuration());

        job.setJarByClass(InverseIndex.class);

        job.setMapperClass(IndexMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        FileInputFormat.setInputPaths(job, new Path(args[0]));

        job.setReducerClass(IndexReduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setCombinerClass(IndexCombiner.class);

        job.waitForCompletion(true);

    }

    /**
     * map
     */
    public static class IndexMapper extends Mapper<LongWritable, Text, Text, Text> {

        private Text k = new Text();
        private Text v = new Text();

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] words = line.split(" ");

            // 获得文件的路径
            FileSplit inputSplit = (FileSplit) context.getInputSplit();
            String path = inputSplit.getPath().toString();
            path = path.substring(path.lastIndexOf("9000") + 4);
            for (String word : words) {

                // "hello->/aa/a.txt"
                k.set(word + "->" + path);
                v.set("1");
                context.write(k, v);
            }
        }
    }

    /**
     * combiner
     */
    public static class IndexCombiner extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String line = key.toString();
            String[] split = line.split("->");

            String word = split[0];
            String path = split[1];

            int count = 0;

            for (Text value : values) {
                count += Integer.parseInt(value.toString());
            }

            context.write(new Text(word), new Text(path + "->" + count));
        }
    }

    /**
     * reduce
     */
    public static class IndexReduce extends Reducer<Text, Text, Text, Text> {

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String result = "";

            for (Text value : values) {
                result += value.toString() + "\t";
            }

            context.write(key, new Text(result));
        }
    }
}

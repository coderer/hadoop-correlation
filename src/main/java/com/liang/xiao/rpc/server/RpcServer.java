package com.liang.xiao.rpc.server;

import com.liang.xiao.rpc.Bizable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.Server;


/**
 * Rpc服务端
 * 2018年4月19日21:38:44
 */
public class RpcServer implements Bizable {

    public static int index = 1;

    public String sayHi(String str) {

        System.out.println("接收到客户端的第" + index + "次请求");

        index += 1;

        return "Hi~," + str;
    }

    public static void main(String[] args) {
        // 注册webserver服务
        Server server = null;
        try {
            server = new RPC.Builder(new Configuration()).setProtocol(Bizable.class).setInstance(new RpcServer()).setBindAddress("192.168.8.1").setPort(9527).build();
            server.start();
            System.out.println("RPC服务已经启动！！！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.liang.xiao.rpc;

/**
 * RPC共用接口
 * 2018年4月19日21:38:56
 */
public interface Bizable {

    public static final long versionID = 10020;

    String sayHi(String str);
}

package com.liang.xiao.rpc.client;

import com.liang.xiao.rpc.Bizable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.net.InetSocketAddress;

/**
 * Rpc客户端
 * 2018年4月19日21:38:51
 */
public class RpcClient {

    public static void main(String[] args) {
        try {
            Bizable proxy = RPC.getProxy(Bizable.class, 10010, new InetSocketAddress("192.168.8.1", 9527), new Configuration());


            for (int i = 0; i < 20; i++) {
                String result = proxy.sayHi("tomcat");
                System.out.println(result + i + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            }


            RPC.stopProxy(proxy);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

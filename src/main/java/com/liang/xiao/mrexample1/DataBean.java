package com.liang.xiao.mrexample1;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @describe: mapReduce计算返回实体类
 * @author: xiaoliang.liu
 * @date: 2018/4/21 18:41  v1.0
 */
public class DataBean implements Writable {

    /**
     * 手机号
     */
    private String telNo;

    /**
     * 上行流量
     */
    private Long upPayLoad;

    /**
     * 下行流量
     */
    private Long downPayLoad;

    /**
     * 上下行总流量
     */
    private Long totalPayLoad;

    public DataBean() {
    }

    public DataBean(String telNo, Long upPayLoad, Long downPayLoad) {
        this.telNo = telNo;
        this.upPayLoad = upPayLoad;
        this.downPayLoad = downPayLoad;
        this.totalPayLoad = upPayLoad + downPayLoad;
    }

    // 序列化
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(telNo);
        dataOutput.writeLong(upPayLoad);
        dataOutput.writeLong(downPayLoad);
        dataOutput.writeLong(totalPayLoad);
    }

    // 反序列化
    public void readFields(DataInput dataInput) throws IOException {
        this.telNo = dataInput.readUTF();
        this.upPayLoad = dataInput.readLong();
        this.downPayLoad = dataInput.readLong();
        this.totalPayLoad = dataInput.readLong();
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public Long getUpPayLoad() {
        return upPayLoad;
    }

    public void setUpPayLoad(Long upPayLoad) {
        this.upPayLoad = upPayLoad;
    }

    public Long getDownPayLoad() {
        return downPayLoad;
    }

    public void setDownPayLoad(Long downPayLoad) {
        this.downPayLoad = downPayLoad;
    }

    public Long getTotalPayLoad() {
        return totalPayLoad;
    }

    public void setTotalPayLoad(Long totalPayLoad) {
        this.totalPayLoad = totalPayLoad;
    }

    @Override
    public String toString() {
        return "{" +
                "upPayLoad=" + upPayLoad +
                ", downPayLoad=" + downPayLoad +
                ", totalPayLoad=" + totalPayLoad +
                '}';
    }
}

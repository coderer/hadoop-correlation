package com.liang.xiao.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;

import java.net.URI;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        try {
            FileSystem fs = FileSystem.get(new URI("hdfs://itcast01:9000"), new Configuration(), "root");

//            // 从hdfs上下载文件
//            InputStream in = fs.open(new Path("/456.txt"));
//            OutputStream out = new FileOutputStream("E://456.txt");
//            IOUtils.copyBytes(in, out, 4096, true);
//
//            // 上传文件到hdfs上
//            InputStream in = new FileInputStream("E:/123.txt");
//            OutputStream out = fs.create(new Path("/456.txt"));
//            IOUtils.copyBytes(in, out, 4096, true);


//            // 从hdfs上下载文件
//            fs.copyToLocalFile(false,new Path("/456.txt"), new Path("E://789.txt"), true);


//            // 删除hdfs上的文件
//            fs.delete(new Path("/456.txt"));
//            // 删除hdfs上的文件夹(递归删除)
//            fs.delete(new Path("/456.txt"), true);

            System.out.println("success");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
package com.liang.xiao.hadoopcluster;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

/**
 * @describe: 操作hadoop集群中的hdfs文件系统
 * @author: xiaoliang.liu
 * @date: 2018/4/30 23:54  v1.0
 */
public class HdfsHaTest {

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        // namenode集群服务器名
        conf.set("dfs.nameservices", "ns1");
        // namenode名
        conf.set("dfs.ha.namenodes.ns1", "nn1,nn2");
        // namenode的rpc地址
        conf.set("dfs.namenode.rpc-address.ns1.nn1", "itcast01:9000");
        conf.set("dfs.namenode.rpc-address.ns1.nn2", "itcast02:9000");
        // 实现类
        conf.set("dfs.client.failover.proxy.provider.ns1", "org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider");

        FileSystem fs = FileSystem.get(new URI("hdfs://ns1"), conf, "root");

        // 上传文件
//        InputStream in = new FileInputStream("c:/abc.txt");
//        OutputStream out = fs.create(new Path("/hdfs_ha"));
//        IOUtils.copyBytes(in, out, 4096, true);

        // 下载文件
        InputStream in = fs.open(new Path("/hdfs_ha"));
        OutputStream out = new FileOutputStream("e:/456.txt");
        IOUtils.copyBytes(in, out, 4096, true);

    }
}
